import logo from './logo.svg';
import './App.css';
import { Button } from 'react-bootstrap';
import { Form } from 'react-bootstrap';
import * as React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'


function App() {
  return (
    <div className="App">
      <h1>Follow The Steps Below</h1>
      <br></br>
      <div className='row'>
        <div className='col-3'><Button>1. Get Administrator Consent</Button></div>
        <div className='col-3'><Button>2. Admin-Sign in</Button></div>
        <div className='col-3'><Button>3. Get Application Token</Button></div>
        <div className='col-3'><Button>4. Add Teams</Button></div>
      </div>
      <br></br><br></br><br></br>
      <Form>
  <Form.Group className="mb-3" controlId="formBasicEmail">
    <Form.Label>MS Teams' ID</Form.Label>
    <Form.Control type="email" placeholder="Enter MS Teams' ID" />
  </Form.Group>

  <Form.Group className="mb-3" controlId="formBasicPassword">
    <Form.Label>Tetherfi team's Name</Form.Label>
    <Form.Control type="password" placeholder="Eneter Tetherfi Team's Name" />
  </Form.Group>
  
  <Button variant="primary" type="submit">
    Submit
  </Button>
</Form>
<br></br><br></br>
<Button variant="secondary" size="lg">
      Next
    </Button>

    </div>
  );
}

export default App;
